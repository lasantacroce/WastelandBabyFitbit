# Wasteland, Baby! (Fitbit clockface)
Credit: Lindsay Santacroce (contact: lindsayas22@gmail.com)

## Description
A Fitbit clockface based on the musician Hozier's Wasteland, Baby! album cover. 

## Features
- Made for a fitbit Versa 2
- The album art background (painting by Raine Hozier-Byrne)  
- A top bar with current battery level, steps, and heart rate  
- Time in the Hozier logo font
- Date in Hozier's handwriting (designed by me using handwriting samples)

## Visual
![Wasteland, Baby! Fitbit clockface](Wasteland%2C%20Baby!%20Clock%20Face%20Screenshot.PNG)  
  
## Disclaimer
I do not own the rights to the Wasteland, Baby! cover art (all credit goes to Raine Hozier-Byrne). This was made for fun and free to use for **personal purposes only**. Hope you love it!
